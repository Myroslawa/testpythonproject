import pytest
import time
from typing import NewType
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from PageObjects.HomePage import HomePage
from PageObjects.CareersPage import CareersPage
from PageObjects.QualityAssurancePage import QualityAssurancePage
from PageObjects.OpenPositionsPage import OpenPositionsPage
from seleniumpagefactory.Pagefactory import PageFactory


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    driver.inplicitly_wait(10)
    driver.close()
    driver.quit()


def test(driver):
    # Go to Home Page
    driver.get("https://useinsider.com/")
    home_page = HomePage(driver)
    home_page.wait_on_home_page()
    getURL= self.driver.current_url
    assert 'https://useinsider.com/' == getURL, "URLs are different"
    home_page.click_on_company_dropdown()

    # Go to Careers Page
    home_page.click_and_redirect_to_careers_page()
    careers_page = CareersPage(driver)
    assertTrue(careers_page.locations_section.is_displayed()), "Location title is not presented"
    assertTrue(careers_page.life_at_insider_section.is_displayed()), "Life at insider title is not presented"

    # Go to QualityAssurancePage Page and open jobs
    driver.get("https://useinsider.com/careers/quality-assurance/")
    quality_assurance_page = QualityAssurancePage(driver)
    quality_assurance_page.click_on_see_all_qa_jobs_href_button()

    # Set up filter by location and department
    open_positions_page = OpenPositionsPage(driver)
    location = "Istanbul, Turkey"
    department = "Quality Assurance"
    open_positions_page.click_on_filter_by_location()
    open_positions_page.select_location_in_filter(location)
    open_positions_page.click_on_filter_by_department()
    open_positions_page.select_department_in_filter(department)
    job_list_title = "No positions available."
    get_job_list_title = open_positions_page.job_list_title.text

    # Check that jobs list is not empty by titles
    assertNotEqual(job_list_title, get_job_list_title, "Title - No positions available.")

    # Check that location and department position match with selected filter
    for job_position_in_list in range(1, 4):
        assert location == position_location(job_position_in_list), "Locations in position are different"
        assert department == position_department(job_position_in_list), "Department in position are different"

    open_positions_page.click_on_view_role_first_button()
    assert open_positions_page.get_position_title_of_selected_job == open_positions_page.get_position_title_first, "Position titles are different"




