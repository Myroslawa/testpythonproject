from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class OpenPositionsPage(PageFactory):
    def __init__(self, driver):
        self.driver = driver

        filter_by_location = driver.find_element_by_xpath("//span[@id = 'select2-filter-by-location-container']")
        select_location_option = driver.find_element_by_xpath("//select[@id = 'filter-by-location']//option[text() = 'Istanbul, Turkey']")
        filter_by_department = driver.find_element_by_xpath("//span[@id = 'select2-filter-by-department-container']")
        select_department_option = driver.find_element_by_xpath("//select[@id= 'filter-by-department']//option[text() = 'Quality Assurance']")
        job_list_title = driver.find_element_by_xpath("//div[@id = 'jobs-list']//*")
        view_role_first_button = driver.find_element_by_xpath("//div[@id = 'jobs-list']//div[1]//div//a")
        position_first_title = driver.find_element_by_xpath("//div[@id = 'jobs-list']//div[1]//div//p")
        position_title_of_selected_job = driver.find_element_by_xpath("//div[@class = 'posting-headline']//h2")

        def click_on_filter_by_location():
            self.filter_by_location.click()

        def select_location_in_filter(location):
            self.driver.find_element_by_xpath("//select//option[text() = '" + location + "']").click()

        def click_on_filter_by_department():
            self.filter_by_department.click()

        def position_location(job_position_in_list):
            self.driver.find_element_by_xpath("//div[@id = 'jobs-list']//div[" + job_position_in_list + "]//div//div").text

        def position_department(job_position_in_list):
            self.driver.find_element_by_xpath("//div[@id = 'jobs-list']//div[" + job_position_in_list + "]//div//span").text

        def click_on_view_role_first_button():
            self.view_role_first_button.click()

        def get_position_title_first():
            self.driver.position_first_title.text

        def get_position_title_of_selected_job():
            self.driver.position_title_of_selected_job.text