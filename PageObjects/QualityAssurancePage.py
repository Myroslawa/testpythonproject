from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class QualityAssurancePage(PageFactory):
    def __init__(self, driver):
        self.driver = driver

        see_all_qa_jobs_href_button = driver.find_element_by_xpath("//a[@href='https://useinsider.com/careers/open-positions/?department=qualityassurance']")

        def click_on_see_all_qa_jobs_href_button():
            self.see_all_qa_jobs_href_button.click()

