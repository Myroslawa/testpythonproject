from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class CareersPage(PageFactory):
    def __init__(self, driver):
        self.driver = driver

        locations_section = driver.find_element_by_xpath("//section[@id = 'career-our-location']")
        life_at_insider_section = driver.find_element_by_xpath("//section[@data-id = 'a8e7b90']")

        def wait_on_locations_section():
            self.locations_section.is_displayed()

        def wait_on_life_at_insider_section():
            self.life_at_insider_section.is_displayed()




