from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class HomePage(PageFactory):
    def __init__(self, driver):
        self.driver = driver

        home_desktop = driver.find_element_by_xpath("//div[@id = 'revamp-head-desktop']")
        company_dropdown = driver.find_element_by_xpath("//li[6]//a[@id= 'navbarDropdownMenuLink']")
        careers_menu_href = driver.find_element_by_xpath("//a[@href='https://useinsider.com/careers/']")

        def wait_on_home_page():
            self.home_desktop.is_displayed()

        def click_on_company_dropdown():
            self.company_dropdown.click()

        def click_and_redirect_to_careers_page():
            self.careers_menu_href.click()


